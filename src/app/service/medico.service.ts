import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Medico } from '../models/medico';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {
  private urlEnpoint = "http://localhost:8080/api/medico";
  private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })

  constructor(private http: HttpClient) { }


  getListaMedicos() {
    return this.http.get<Medico[]>(this.urlEnpoint);
  }
}
