import { Injectable } from '@angular/core';
import { OrdemServicoExame } from '../models/ordem-servico-exame';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class OrdensServicoExameService {

  private urlEnpoint = "http://localhost:8080/api/ordemServicoExame";
  private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })


  constructor(private http: HttpClient) { }

  getOrdensServicoExame(): Observable<OrdemServicoExame[]> {
    return this.http.get<OrdemServicoExame[]>(this.urlEnpoint);
  }


  public create(ordem: OrdemServicoExame): Observable<OrdemServicoExame> {
    return this.http.post<OrdemServicoExame>(this.urlEnpoint, ordem, { headers: this.httpHeaders }).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }

        swal.fire('Houve um erro na alta de ordem de servico!', e.error, 'error');
        return throwError(e);
      })
    )
  }

  update(id): Observable<OrdemServicoExame> {
    return this.http.get<OrdemServicoExame>(`${this.urlEnpoint}/${id}`).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        swal.fire('Houve um erro ao atualizar a alta de ordem de servico!', e.error, 'error');
        return throwError(e);
      })
    )
  }

  delete(id: number): Observable<OrdemServicoExame> {
    return this.http.delete<OrdemServicoExame>(`${this.urlEnpoint}/${id}`, { headers: this.httpHeaders }).pipe(
      catchError(e => {
        swal.fire('Houve um erro ao eliminar a de ordem de servico!', e.error, 'error');
        return throwError(e);
      })
    )
  }


}
