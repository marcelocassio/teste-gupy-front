import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PostoColeta } from '../models/posto-coleta';

@Injectable({
  providedIn: 'root'
})
export class PostoColetaService {

  private urlEnpoint = "http://localhost:8080/api/posto-coleta";
  private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })

  constructor(private http: HttpClient) { }

  getListaPostosColeta(){
    return this.http.get<PostoColeta[]>(this.urlEnpoint);
  }
}
