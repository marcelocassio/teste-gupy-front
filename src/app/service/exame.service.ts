import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Exame } from '../models/exame';

@Injectable({
  providedIn: 'root'
})
export class ExameService {
  private urlEnpoint = "http://localhost:8080/api/exame";
  private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })

  constructor(private http: HttpClient) { }

  getListaExames(): Observable<Exame[]> {
    return this.http.get<Exame[]>(this.urlEnpoint);
  }
}
