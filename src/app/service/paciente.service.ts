import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Paciente } from '../models/paciente'
import { Observable, throwError } from 'rxjs';
import { catchError} from 'rxjs/operators';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  private urlEnpoint = "http://localhost:8080/api/paciente";
  private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' })

  constructor(private http: HttpClient, private router: Router) { }



  getPaciente (id: number): Observable<Paciente>{
    return this.http.get<Paciente>(`${this.urlEnpoint}/${id}`).pipe(
      catchError(e =>{
        console.error(e.error)
        swal.fire('O paciente nao existe!');
        return throwError(e);
      })
    )
  }
 
}
