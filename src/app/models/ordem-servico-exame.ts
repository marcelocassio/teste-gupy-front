import { Exame } from "./exame";
import { Medico } from "./medico";
import { OrdemServico } from "./ordem-servico";
import { Paciente } from "./paciente";
import { PostoColeta } from "./posto-coleta";

export class OrdemServicoExame {

    
    id: number;
    ordemServico = new OrdemServico;
    exame = new Exame;
    preco: string;


}
