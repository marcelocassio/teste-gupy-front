export class Paciente {
    id: number;
    nome: string;
    dataNascimento: Date;
    sexo: string;
    endereco: string;
}
