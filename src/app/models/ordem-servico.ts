import { Medico } from "./medico";
import { Paciente } from "./paciente";
import { PostoColeta } from "./posto-coleta";

export class OrdemServico {

    id: number
    data: Date;
    paciente = new Paciente;
    convenio: string;
    postoColeta = new PostoColeta;
    medico = new Medico;

}
