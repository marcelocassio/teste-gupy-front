import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { OrdensServicoExameComponent } from './ordens-servico-exame/ordens-servico-exame.component';
import { OrdensServicoExameService } from './service/ordens-servico-exame.service'
import { HttpClientModule } from '@angular/common/http';
import { FormComponent } from './ordens-servico-exame/form.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    OrdensServicoExameComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [OrdensServicoExameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
