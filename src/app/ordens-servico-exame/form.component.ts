import { Component, OnInit } from '@angular/core';
import { OrdemServicoExame } from '../models/ordem-servico-exame';
import { OrdensServicoExameService } from '../service/ordens-servico-exame.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Exame } from '../models/exame';
import { ExameService } from '../service/exame.service';
import { MedicoService } from '../service/medico.service';
import { Medico } from '../models/medico';
import { PostoColeta } from '../models/posto-coleta';
import { PostoColetaService } from '../service/posto-coleta.service';
import { PacienteService } from '../service/paciente.service'
import swal from 'sweetalert2';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  
  exames: Exame[] = [];
  medicos: Medico[] = [];
  postosColeta: PostoColeta[] = [];
  ordem: OrdemServicoExame = new OrdemServicoExame();
  errors: string[];

  constructor(private ordensService: OrdensServicoExameService, private router: Router, private exameService: ExameService, private medicoService: MedicoService,
    private postoColetaService: PostoColetaService, private pacienteService: PacienteService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.exameService.getListaExames().subscribe(
      exames => this.exames = exames
    )

    this.medicoService.getListaMedicos().subscribe(
      medicos => this.medicos = medicos
    )

    this.postoColetaService.getListaPostosColeta().subscribe(
      postosColeta => this.postosColeta = postosColeta
    )

    this.pushOrdem();
  }

  getPaciente(): void {
      this.pacienteService.getPaciente(this.ordem.ordemServico.paciente.id).subscribe(
        paciente => this.ordem.ordemServico.paciente = paciente
      )
     
  }


  pushOrdem(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.ordensService.update(id).subscribe((ordem) => this.ordem = ordem)
      }
    }
    )
  }

  public create(): void {
    this.ordensService.create(this.ordem)
    .subscribe(response => {
        this.router.navigate(["/listar"])
        swal.fire('Nova ordem', 'Ordem de exame salvo corretamente!', 'success');
      },
      err => {
        this.errors = err.error.errors as string[];
        console.error(err.error.errors);
        console.error(err.status);
        
      }
    );
    

  }






}
