import { Component, OnInit } from '@angular/core';
import { OrdemServicoExame } from '../models/ordem-servico-exame';
import { OrdensServicoExameService } from '../service/ordens-servico-exame.service';
import { Medico } from '../models/medico';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-ordens-servico-exame',
  templateUrl: './ordens-servico-exame.component.html',
  styleUrls: ['./ordens-servico-exame.component.css']
})
export class OrdensServicoExameComponent implements OnInit {

  ordens: OrdemServicoExame[] = [];
  ordem: OrdemServicoExame = new OrdemServicoExame;
  medicos: Medico[] = [];

  constructor(private ordemServicoExamenService: OrdensServicoExameService, private http: HttpClient, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.ordemServicoExamenService.getOrdensServicoExame().subscribe(
      ordens => this.ordens = ordens
    );
  }


  delete(ordemServicoExame: OrdemServicoExame): void{
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Voce tem certeza??',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Nao',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.ordemServicoExamenService.delete(ordemServicoExame.id).subscribe(
          response => {
            this.ordens =  this.ordens.filter(or => or !== ordemServicoExame)
            swalWithBootstrapButtons.fire(
              'Eliminado!',
              'A ordem de exame foi eliminada corretamente.',
              'success'
            )
          }
        )
      }
    })
  }
}
