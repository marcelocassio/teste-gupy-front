import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './ordens-servico-exame/form.component';
import { OrdensServicoExameComponent } from './ordens-servico-exame/ordens-servico-exame.component';

const routes: Routes = [
  {path: 'listar', component: OrdensServicoExameComponent},
  {path: 'alta', component: FormComponent},
  {path: 'ordem/form/:id', component: FormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
